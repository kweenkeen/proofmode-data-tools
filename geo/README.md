# Overview

proofToGeo is a command line tool (and/or potential js library) for transforming folders of ProofMode data into GeoJSON or KML geo formats

# How To

> node proofToGeo file-path-to-root-folder export-format:geojson/kml

# Example

node proofToGeo.js ProofCorps//Skatepark\ from\ Cluj-Napoca,\ Romania geojson

{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Point","coordinates":[23.552770198176283,46.76434326171875]},"properties":{"name":"Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time","hash":"5a80a76cc800ceb9e98e977849ad4ed71f8524bc521e0766cfca21d7ddf6dac3","device":"iPhone"}},{"type":"Feature","geometry":{"type":"Point","coordinates":[23.552770198176283,46.76434326171875]},"properties":{"name":"Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time","hash":"5a80a76cc800ceb9e98e977849ad4ed71f8524bc521e0766cfca21d7ddf6dac3","device":"iPhone"}},{"type":"Feature","geometry":{"type":"LineString","coordinates":[[23.552770198176283,46.76434326171875],[23.552770198176283,46.76434326171875]]},"properties":{}},{"type":"Feature","geometry":{"type":"Point","coordinates":[23.552770198176283,46.76434326171875]},"properties":{"name":"Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time","hash":"5a80a76cc800ceb9e98e977849ad4ed71f8524bc521e0766cfca21d7ddf6dac3","device":"iPhone"}},{"type":"Feature","geometry":{"type":"LineString","coordinates":[[23.552770198176283,46.76434326171875],[23.552770198176283,46.76434326171875]]},"properties":{}},{"type":"Feature","geometry":{"type":"Point","coordinates":[23.55267898941951,46.764404296875]},"properties":{"name":"Saturday, 21 January 2023 at 12:00:56 Eastern European Standard Time","hash":"5fd4cbb3985a206344cd74a6bc557373da0782f89c16824604d4c015c660c73a","device":"iPhone"}}....

node proofToGeo.js ProofCorps//Skatepark\ from\ Cluj-Napoca,\ Romania kml

&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&lt;kml xmlns=&quot;http://www.opengis.net/kml/2.2&quot;&gt;&lt;Document&gt;&lt;Placemark&gt;&lt;name&gt;Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time&lt;/name&gt;&lt;ExtendedData&gt;&lt;Data name=&quot;name&quot;&gt;&lt;value&gt;Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time&lt;/value&gt;&lt;/Data&gt;&lt;Data name=&quot;hash&quot;&gt;&lt;value&gt;5a80a76cc800ceb9e98e977849ad4ed71f8524bc521e0766cfca21d7ddf6dac3&lt;/value&gt;&lt;/Data&gt;&lt;Data name=&quot;device&quot;&gt;&lt;value&gt;iPhone&lt;/value&gt;&lt;/Data&gt;&lt;/ExtendedData&gt;&lt;Point&gt;&lt;coordinates&gt;23.552770198176283,46.76434326171875&lt;/coordinates&gt;&lt;/Point&gt;&lt;/Placemark&gt;&lt;Placemark&gt;&lt;name&gt;Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time&lt;/name&gt;&lt;ExtendedData&gt;&lt;Data name=&quot;name&quot;&gt;&lt;value&gt;Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time&lt;/value&gt;&lt;/Data&gt;&lt;Data name=&quot;hash&quot;&gt;&lt;value&gt;5a80a76cc800ceb9e98e977849ad4ed71f8524bc521e0766cfca21d7ddf6dac3&lt;/value&gt;&lt;/Data&gt;&lt;Data name=&quot;device&quot;&gt;&lt;value&gt;iPhone&lt;/value&gt;&lt;/Data&gt;&lt;/ExtendedData&gt;&lt;Point&gt;&lt;coordinates&gt;23.552770198176283,46.76434326171875&lt;/coordinates&gt;&lt;/Point&gt;&lt;/Placemark&gt;&lt;Placemark&gt;&lt;ExtendedData&gt;&lt;/ExtendedData&gt;&lt;LineString&gt;&lt;coordinates&gt;23.552770198176283,46.76434326171875 23.552770198176283,46.76434326171875&lt;/coordinates&gt;&lt;/LineString&gt;&lt;/Placemark&gt;&lt;Placemark&gt;&lt;name&gt;Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time&lt;/name&gt;&lt;ExtendedData&gt;&lt;Data name=&quot;name&quot;&gt;&lt;value&gt;Saturday, 21 January 2023 at 12:00:14 Eastern European Standard Time&lt;/value&gt;&lt;/Data&gt;&lt;Data name=&quot;hash&quot;&gt;&lt;value&gt;5a80a76cc800ceb9e98e977849ad4ed71f8524bc521e0766cfca21d7ddf6dac3&lt;/value&gt;&lt;/Data&gt;&lt;Data name=&quot;device&quot;&gt;&lt;value&gt;iPhone&lt;/value&gt;&lt;/Data&gt;&lt;/ExtendedData&gt;&lt;Point&gt;&lt;coordinates&gt;23.552770198176283,46.76434326171875&lt;/coordinates&gt;&lt;/Point&gt;&lt;/Placemark&gt;&lt;Placemark&gt;&lt;ExtendedData&gt;&lt;/ExtendedData&gt;&lt;LineString&gt;&lt;coordinates&gt;23.552770198176283,46.76434326171875 23.552770198176283,46.76434326171875&lt;/coordinates&gt;&lt;/LineString&gt;&lt;/Placemark&gt;&lt;Placemark&gt;&lt;name&gt;Saturday, 21 January 2023 at 12:00:56 Eastern European Standard Time&lt;/name&gt;&lt;ExtendedData&gt;&lt;Data name=&quot;name&quot;&gt;&lt;value&gt;Saturday, 21 January 2023 at 12:00:56 Eastern European Standard Time&lt;/value&gt;&lt;/Data&gt;&lt;Data name=&quot;hash&quot;&gt;&lt;value&gt;5fd4cbb3985a206344cd74a6bc557373da0782f89c16824604d4c015c660c73a&lt;/value&gt;&lt;/Data&gt;&lt;Data name=&quot;device&quot;&gt;&lt;value&gt;iPhone&lt;/value&gt;&lt;/Data&gt;&lt;/ExtendedData&gt;&lt;Point&gt;&lt;coordinates&gt;23.55267898941951,46.764404296875&lt;/coordinates&gt;&lt;/Point&gt;&lt;/Placemark&gt;&lt;Placemark&gt;&lt;ExtendedData&gt;...

# Contributors

 * Nathan Freitas @n8fr8 nathan@guardianproject.info

# Dependencies
 * maphubs tokml library
 * geojson library

