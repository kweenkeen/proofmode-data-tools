
var fs = require('fs');
var path = require('path');
// In newer Node.js versions where process is already global this isn't necessary.
var process = require("process");
var geojson = require('geojson');
const tokml = require('tokml');

var proofDir = process.argv[2]
var exportType = process.argv[3]
const proofPoints = [];
var lastPoint;

function getProof (targetDir, isLast) {

fs.readdirSync(targetDir).forEach(file => {
    // Make one pass and make the file complete
    var fromPath = path.join(targetDir, file);
 
      if (fromPath.endsWith(".proof.json"))
      {
	let rawdata = fs.readFileSync(fromPath);
	let proof = JSON.parse(rawdata);
 
        var thisPoint = {name: proof["File Modified"], hash: proof["File Hash SHA256"], device: proof["Hardware"], latitude: parseFloat(proof["Location.Latitude"]), longitude: parseFloat(proof["Location.Longitude"])}
        if (thisPoint.latitude && thisPoint.longitude) {

        proofPoints.push(thisPoint);

        if (lastPoint && lastPoint.longitude && lastPoint.latitude )
        	proofPoints.push({line: [[lastPoint.longitude, lastPoint.latitude], [thisPoint.longitude, thisPoint.latitude]]})

        lastPoint = thisPoint;
        }
      }
      else if (fs.lstatSync(fromPath).isDirectory())
      {
        getProof(fromPath)
      }	
 

  });
}

function generateKml (points) {

  const geojsonObject = geojson.parse(points, { Point: ['latitude', 'longitude'], LineString: 'line' })
  if (exportType == "geojson")
  { 
  	console.log(JSON.stringify(geojsonObject));
  }
  else
  {
  	const response = tokml(geojsonObject);
  	console.log((response));
  }
}

function proofKml () {

  if (proofDir)
  {
     getProof(proofDir, true);
     generateKml(proofPoints);
  } 
  else
  {
    console.log("proofToGeo <path-to-proof-directory-base> <export-type:geojson or kml>")
  }
}

proofKml();
