import numpy as np
import pandas as pd
import altair as alt
import json
import re

name_of_download = input('Type in the exact name of the resulting JSON file for ProofMode data: ')

def renaming_function_file(name_of_download):
    df = pd.read_json(name_of_download)
    new_column_names = []
    for (columnName, columnData) in df.iteritems():
        columnName_split = columnName.replace(".", " ")
        for ind, char in enumerate(columnName[:-1]):
            if columnName[ind].islower() and columnName[ind+1].isupper():
                columnName_split = columnName[:ind +
                                              1] + ' ' + columnName[ind+1:]
        new_column_names.append(columnName_split)

    df.columns = new_column_names
    return df

def change_df_index(df):
    photo_df = df.copy(deep=True)
    photo_df['File Name'] = ''
    for index, row in photo_df.iterrows():
        file_name = row["File Path"].split("/")[-1]
        photo_df.at[index, 'File Name'] = file_name
    photo_df = photo_df.set_index('File Name')
    # print(photo_df)
    return photo_df

def use_file_name_as_outer_key(photo_df):
    photo_dict = photo_df.to_dict('index')
    # print(photo_dict)
    return(photo_dict)

def change_dict_to_pandas_df(photo_dict):
    nested_photo_df = pd.DataFrame.from_dict(photo_dict, orient = 'index')
    print(nested_photo_df)
    return nested_photo_df


df = renaming_function_file(name_of_download)
photo_df = change_df_index(df)
photo_dict = use_file_name_as_outer_key(photo_df)
nested_photo_df = change_dict_to_pandas_df(photo_dict)