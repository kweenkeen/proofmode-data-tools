from dateutil.parser import parse
import pandas as pd
import geopandas
import geopy.distance
import folium
import json
import os
from geographiclib.geodesic import Geodesic


def synchrony_time_elapsed(pj):
    df = pd.read_json(pj)
    list_of_times = []

    for index, row in df.iterrows():
        raw = df.loc[index, "Proof Generated"]
        try:
            file_timestamp = parse(raw, fuzzy=True)
            list_of_times.append(file_timestamp)
        except Exception as e:
            print(e)

    time_initial = min(list_of_times)
    time_final = max(list_of_times)
    time_elapsed = time_final - time_initial
    return df, time_elapsed

def create_geo_df(df):
    geo_df = df[["DeviceID", "Location.Provider", "Location.Latitude", "Location.Longitude"]]
    geo_df["Location.Longitude"] = pd.to_numeric(geo_df["Location.Longitude"])
    geo_df["Location.Latitude"] = pd.to_numeric(geo_df["Location.Latitude"])
    geo_df = geo_df.dropna()
    geo_df = geo_df.drop_duplicates()
    return geo_df


def synchrony_difference_between_points_in_km(geo_df):
  differences_km = []
  coords_set_a = []
  coords_set_b = []
  for i, row in geo_df.iterrows():
    latitude_i = geo_df.loc[i]['Location.Latitude']
    longitude_i = geo_df.loc[i]['Location.Longitude']
    for j, row in geo_df.iterrows():
      latitude_j = geo_df.loc[j]['Location.Latitude']
      longitude_j = geo_df.loc[j]['Location.Longitude']
      if latitude_i != latitude_j and longitude_i != longitude_j:
        coords_i = (latitude_i, longitude_i)
        coords_j = (latitude_j, longitude_j)
        diff_between_one_set_of_points_km = geopy.distance.geodesic(coords_i, coords_j).km
        differences_km.append(diff_between_one_set_of_points_km)
        coords_set_a.append(coords_i)
        coords_set_b.append(coords_j)
  list_of_coordinates_and_differences = list(zip(coords_set_a, coords_set_b, differences_km))
  differences_km_df = pd.DataFrame(list_of_coordinates_and_differences, columns = ['Coordinates Set A', 'Coordinates Set B', 'Differences in km'])
  max_radius = differences_km_df['Differences in km'].max()
  coords_max_difference_df = differences_km_df[differences_km_df['Differences in km'] == differences_km_df['Differences in km'].max()]
  coords_max_a = coords_max_difference_df['Coordinates Set A'].loc[coords_max_difference_df.index[0]]
  coords_max_lat_a, coords_max_lon_a = coords_max_a
  coords_max_b = coords_max_difference_df['Coordinates Set B'].loc[coords_max_difference_df.index[0]]
  coords_max_lat_b, coords_max_lon_b = coords_max_b
  return differences_km_df, max_radius, coords_max_difference_df, coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b


def synchrony_centroid_map(geo_df, max_radius, coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b):
    geojson_df = geopandas.GeoDataFrame(geo_df, geometry=geopandas.points_from_xy(
        geo_df["Location.Latitude"], geo_df['Location.Longitude']))
    x_list = geojson_df['Location.Latitude']
    y_list = geojson_df['Location.Longitude']
    # centroid_of_coordinates = geojson_df.dissolve().centroid
    # x_centroid = centroid_of_coordinates.centroid.x
    # y_centroid = centroid_of_coordinates.centroid.y

    path_from_a_to_b = Geodesic.WGS84.Inverse(coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b)

    midpoint_array = Geodesic.WGS84.Direct(coords_max_lat_a, coords_max_lon_a, path_from_a_to_b['azi1'], path_from_a_to_b['s12']/2)
    
    midpoint_lat = midpoint_array['lat2']
    midpoint_lon = midpoint_array['lon2']

    centroid_map = folium.Map(location=[midpoint_lat, midpoint_lon])

    folium.Circle([midpoint_lat, midpoint_lon],
                    radius=max_radius/2*1000
                   ).add_to(centroid_map)

    all_coordinates = zip(x_list, y_list)
    for i in all_coordinates:
        folium.Marker(i, popup="Proof Point", icon=folium.Icon(
            color='green')).add_to(centroid_map)

    sw = geo_df[['Location.Latitude', 'Location.Longitude']].min().values.tolist()
    ne = geo_df[['Location.Latitude', 'Location.Longitude']].max().values.tolist()
    centroid_map.fit_bounds([sw, ne]) 

    return geojson_df, centroid_map

def check_synchrony(pj):
    df, time_elapsed = synchrony_time_elapsed(pj)
    geo_df = create_geo_df(df)
    differences_km_df, max_radius, coords_max_difference_df, coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b = synchrony_difference_between_points_in_km(geo_df)
    geojson_df, centroid_map = synchrony_centroid_map(geo_df, max_radius, coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b)
    if not os.path.exists('outputs'):
        os.makedirs('outputs')
    centroid_map.save(outfile='outputs/centroid_map.html')
    with open("outputs/centroid_map.html", "r", encoding='utf8') as html_file:
        centroid_map_html = ""
        for readline in html_file:
            line_strip = readline.strip()
            centroid_map_html += line_strip
    return json.dumps({
        "time_elapsed": time_elapsed.total_seconds(),
        "geojson": json.loads(geojson_df.to_json()),
        "differences_km_df": json.loads(differences_km_df.to_json()),
        "centroid_map_html": centroid_map_html
    })