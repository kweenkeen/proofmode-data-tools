import csv
from datetime import date
import glob
import os
import pandas as pd
import zipfile
import json
import py7zr
import shutil
from pyunpack import Archive
# from exif import Image
from os import listdir
from PIL import Image, ExifTags
import glob
from collections import defaultdict
from PIL import Image, ExifTags, TiffImagePlugin
from pillow_heif import register_heif_opener
import ntpath
ntpath.basename("a/b/c")

# import magic

# CREATE NEW JSON OF CONCATENATED PROOFS FROM EACH INDIVIDUAL JSON FILE
register_heif_opener()

todays_date = str(date.today())

# READ IN FILES

name_of_download = input('Type in the exact name of the folder you downloaded: ')
JSON = '.json'

def unzip_proof(PATH_TO_FOLDER, PATH_TO_ZIP):

    subdirectory_to_extract_to = PATH_TO_FOLDER + '/unzipped_proof'
    if not os.path.exists(subdirectory_to_extract_to):
        os.makedirs(subdirectory_to_extract_to)
    ## READ IN NEW PROOF

    for path, subdir, files in os.walk(PATH_TO_ZIP):
        if files:
            for file in files:
                if zipfile.is_zipfile(file):
                    path_of_file = os.path.join(path, file)
                    with zipfile.ZipFile(path_of_file, 'r') as zip_ref:
                        zip_ref.extractall(subdirectory_to_extract_to)
    return subdirectory_to_extract_to

def batch_JSON(unzipped_directory):
    newfilename = "ProofMode_Collection_" + todays_date + JSON
    batch = list()
    for proof_file in glob.glob(unzipped_directory + "/**/*" + JSON, recursive=True):
        with open(proof_file, 'r') as input_file:
            batch.append(json.load(input_file))

    with open(newfilename, 'w') as output_file:
        json.dump(batch, output_file, indent=4, sort_keys=True)
        output_file.close()

def exif_extraction(unzipped_directory):
    newfilename = "EXIF_data_" + todays_date + JSON
    exif_data_dict = {}

    for image_file in glob.glob(unzipped_directory+ "/**/*", recursive = True):
        
        if (image_file.endswith(".jpg")) or (image_file.endswith(".jpeg")) or (image_file.endswith(".png")) or (image_file.endswith(".HEIC")):
            image_open = Image.open(image_file)
            image_open.verify()
            image_getexif = image_open.getexif()
            if image_getexif:
                head, tail = ntpath.split(image_file)
                for (k, v) in image_getexif.items():
                    exif = {
                        ExifTags.TAGS[k]: v for k, v in image_getexif.items() if k in ExifTags.TAGS and type(v) not in [bytes, TiffImagePlugin.IFDRational]
                    }
                    exif_data_dict[tail] = exif

    with open(newfilename, 'w') as output_file:
        json_exif_data = json.dump(exif_data_dict, output_file, indent=4, sort_keys=True)
        output_file.close()

    return json_exif_data

if '.zip' in name_of_download:
    PATH_TO_ZIP = '/Users/jackfoxkeen/Desktop/ProofMode/ProofModeFiles/' + name_of_download
    PATH_TO_FOLDER = PATH_TO_ZIP.replace('.zip','')
    subdirectory_to_extract_to = unzip_proof(PATH_TO_FOLDER, PATH_TO_ZIP)
    batch_JSON(subdirectory_to_extract_to)
    exif_extraction(subdirectory_to_extract_to)

else:
    PATH_TO_FOLDER = '/Users/jackfoxkeen/Desktop/ProofMode/ProofModeFiles/' + name_of_download
    batch_JSON(PATH_TO_FOLDER)
    exif_extraction(PATH_TO_FOLDER)