import numpy as np
import pandas as pd
import altair as alt
import json

# pj = input('Type in the exact name of the resulting JSON file for ProofMode data: ')

def renaming_function_file(pj):
    df = pd.read_json(pj)
    new_column_names = []
    for (columnName, columnData) in df.iteritems():
        columnName_split = columnName.replace(".", " ")
        for ind, char in enumerate(columnName[:-1]):
            if columnName[ind].islower() and columnName[ind+1].isupper():
                columnName_split = columnName[:ind +
                                              1] + ' ' + columnName[ind+1:]
        new_column_names.append(columnName_split)

    df.columns = new_column_names
    return df


def binning_function(df):
    df.replace("", np.nan, inplace=True)
    # Create list of titles
    metadata_list = df.columns

    # Initiate device ID dataframe
    DeviceID_List = []

    for i in list(df['Device ID']):
        if i not in DeviceID_List:
            DeviceID_List.append(i)

    # Create new dataframe for pivot table, with Unique Device IDs as the column headers, and hardware/software features as the row index
    device_df = pd.DataFrame(columns=DeviceID_List, index=metadata_list)

    device_data = {}

    # Group number of each unique feature by device ID.
    # (For example, if there are two language per device ID, count "2")
    # For most features, there will be 1 feature per device ID.

    for i, row in device_df.iterrows():
        count_of_uniqueness = df.groupby("Device ID")[i].nunique()
        # device_df = device_df.append(aggregate_df)
        device_data[i] = count_of_uniqueness

    device_data_df = pd.DataFrame.from_dict(device_data, orient='index')

    # This is a REALLY SLOPPY way of doing this, but it's what worked quickest so I'm leaving it for now.
    # If you're translating this into JavaScript, a better option would be to replace each line based on the index

    device_data_df = device_data_df.dropna()

    # In a future version of the code, I'm going to change the initiation of the DeviceID Dataframe to avoid having to transpose it later on.
    # Trial and error process of working with the histogram and the pivot table, sorry about that!
    transpose_df = device_data_df.T

    return transpose_df


def outliers_histogram_robust(df, transpose_df):

    # Create empty list to store device IDs (this is a string)
    list_of_device_IDs = []

    # Create empty list to store occurences of device ID (this is an integer)
    list_of_device_ID_occurences = []

    # Create list of attributes which should have a 1:1 correspondence--one attribute per photo
    list_of_attributes_which_have_1_to_1_correspondence = [
        'File Hash SHA256', 'File Modified', 'File Path', 'Proof Generated']  # removed 'SafetyCheck Timestamp' - it's an optional field and was causing an error

    # Create a list for location attributes that will vary by picture
    list_of_varying_location_attributes = [
        'Location Accuracy', 'Location Latitude',  'Location Longitude', 'Location Time']

    # Create empty list for other attributes
    list_of_non_1_to_1_occurences = []

    outlier_dict = {}

    # For the column Device ID, iterate though the index of the column entry (the device ID)
    for label, content in transpose_df.iteritems():

        list_of_non_1_to_1_occurences.append(label)

        # Go through the integer number of attributes in the content.values column
        # for number_attributes in content.values:

        if label == 'Device ID':

            # Iterate through Device ID listed in content.index
            for j in content.index:

                # Add Device ID to list of device IDs
                list_of_device_IDs.append(j)

                # Count the number of occcurences of the device ID -- this should be the number of photos
                list_of_device_ID_occurences.append(
                    df['Device ID'].value_counts()[j])

        # Create a dictionary from these two lists
        device_occurence_dict = dict(
            zip(list_of_device_IDs, list_of_device_ID_occurences))

    # Iterate through list of attributes which have a 1:1 correspondence

    # for k in list_of_attributes_which_have_1_to_1_correspondence:
        # Remove the 1:1 attributes to create "list of non 1:1 attributes"
        # list_of_non_1_to_1_occurences.remove(k)

        # Initialize nested normal dictionary (attributes which are as expected)
    normal_dict = {}

    # Initialize nested outlier dictionary (attributes which are unexpected)
    outlier_dict = {}

    i = 0

    # Iterate through each device (key) and the number of photos (value)
    for key, value in device_occurence_dict.items():

        # Initialize inner normal dictionary for each device ID
        normal_dict[key] = {}

        # Initialize inner outlier dictionary for each device ID
        outlier_dict[key] = {}

        # Iterate through each attribute which has a 1:1 correspondence
        for k in list_of_attributes_which_have_1_to_1_correspondence:

            # For each individual device, check if the number of unique occurences of an attribute per device (transpose_df.iloc[i][k]) equals the number of photos per device (value)
            if transpose_df.index[i] == key and transpose_df.iloc[i][k] == value:

                # If so, for this device and this attribute, add the number of unique occurences of the attribute to the normal dictionary
                normal_dict[key][k] = transpose_df.iloc[i][k]

            else:
                # If not, for this device and this attribute, add the number of unique occurences of the attribute to the outlier dictionary
                outlier_dict[key][k] = transpose_df.iloc[i][k]

        # Iterate through attributes which should have non 1:1 occurences
        for k in list_of_non_1_to_1_occurences:

            if k == "Notes":

                # Check if the number of unique occurences for "Notes" is 0
                if transpose_df.index[i] == key and transpose_df.iloc[i][k] == 0:

                    # If so, for this device and this attribute, add the number of unique occurences (0) to the normal dictionary
                    normal_dict[key][k] = transpose_df.iloc[i][k]
                else:

                    # If not, for this device and this attribute, add the number of unique occurences (>0) to the outlier dictionary
                    outlier_dict[key][k] = transpose_df.iloc[i][k]

            # elif k == "Cell Info":
            #   if df["Device ID"] == key

            elif k in list_of_varying_location_attributes:

                # Check if the number of unique occurences for varying location data is 0 (meaning there is no data for these attributes)
                if transpose_df.index[i] == key and transpose_df.iloc[i][k] == 0:

                    # If so, for this device and this attribute, add the number of unique occurences (0) to the outlier dictionary
                    outlier_dict[key][k] = transpose_df.iloc[i][k]
                else:

                    # If not, for this device and this attribute, add the number of unique occurences (>0) to the normal dictionary
                    normal_dict[key][k] = transpose_df.iloc[i][k]

            # For each individual device, check if the number of unique occurences for the attribute is 1 (e.g. for ScreenSize, we would expect only 1 unique occurence of this attribute across all photos)
            elif transpose_df.index[i] == key and transpose_df.iloc[i][k] == 1:

                # If so, for this device and this attribute, add the number of unique occurences (1) to the normal dictionary
                normal_dict[key][k] = transpose_df.iloc[i][k]
            else:

                # If not, for this device and this attribute, add the number of unique occurences (0 or greater than 1) to the normal dictionary
                outlier_dict[key][k] = transpose_df.iloc[i][k]
        i += 1

    outliers_by_device_id_nested_df = pd.DataFrame.from_dict(
        outlier_dict, orient='index')
    outlier_list = outliers_by_device_id_nested_df.columns.values.tolist()

    return outliers_by_device_id_nested_df, outlier_dict, list_of_device_ID_occurences, device_occurence_dict, outlier_list


def interactive_histogram(outliers_by_device_id_nested_df, outlier_list):

    source = outliers_by_device_id_nested_df
    selector = alt.selection_single(fields=['Device Attribute'], bind='legend')

    base = alt.Chart(source).properties(
        width=260,
        height=260
    ).transform_fold(
        outlier_list,
        as_=['Device Attribute', 'Unique Devices']
    ).add_selection(
        selector
    )

    chart = base.mark_bar().encode(
        x=alt.X('Unique Devices:Q', axis=alt.Axis(
            title='Number of Attributes per Device', tickMinStep=1)),
        y=alt.Y('count()', stack=None, axis=alt.Axis(
            title='Number of Unique Devices', tickMinStep=1)),
        color=alt.Color('Device Attribute:N',
                        scale=alt.Scale(scheme='category20b'),
                        legend=alt.Legend(
                            orient='none',
                            legendX=94, legendY=300,
                            direction='horizontal',
                            titleAnchor='middle')),
        opacity=alt.condition(selector, alt.value(1), alt.value(0.03))
    ).properties(
        title='Histogram of Attributes Across Devices'
    )

    chart.configure_title(
        fontSize=20,
        font='Tahoma',
        anchor='start',
        color='grey')

    return chart


def summary_of_flags(outliers_by_device_id_nested_df):
    df_summary_of_flags = pd.DataFrame()
    df_summary_of_flags['Number of Flags'] = outliers_by_device_id_nested_df.count(
        axis='columns')
    json_summary_of_flags_by_device = df_summary_of_flags.to_json(
        orient="split")
    return df_summary_of_flags, json_summary_of_flags_by_device


def change_df_index(df):
    for index, row in df.iterrows():
        file_name = row["File Path"].split("/")[-1]
        df.at[index, 'File Name'] = file_name
    photo_df = df.set_index('File Name')
    return photo_df


def outliers_dataframe_by_photo_robust(photo_df):

    # Create a list for location attributes that will vary by picture
    list_of_varying_attributes_per_photo = ['Location Provider', 'Location Accuracy', 'Location Latitude',
                                            'Location Longitude', 'Location Time', 'Location Altitude', 'Location Bearing', 'Location Speed']

    # Create a list for attributes that should NOT vary by picture

    list_of_static_attributes_per_photo = ['Hardware', 'Manufacturer', 'Screen Size', 'Language',
                                           'Locale', 'IPv4', 'IPv6', 'Network', 'Network Type', 'Data Type', 'Cell Info', 'Location Provider',]
    # removed 'Wifi MAC',  'SafetyCheckBasic Integrity', 'SafetyCheckCts Match' since they are optional.

    # Initialize nested normal dictionary (attributes which are as expected)
    normal_dict = {}

    # Initialize nested outlier dictionary (attributes which are unexpected)
    outlier_dict = {}

    i = 0

    # Iterate through each device (key) and the number of photos (value)

    for photo, row in photo_df.iterrows():

        # Initialize inner normal dictionary for each device ID
        normal_dict[photo] = {}

        # Initialize inner outlier dictionary for each device ID
        outlier_dict[photo] = {}

        for varying_attribute in list_of_varying_attributes_per_photo:

            if pd.isnull(photo_df.loc[photo, varying_attribute]):
                outlier_dict[photo][varying_attribute] = "Data not captured"

            else:
                normal_dict[photo][varying_attribute] = photo_df.loc[photo,
                                                                     varying_attribute]

        for static_attribute in list_of_static_attributes_per_photo:

            if pd.isnull(photo_df.loc[photo, static_attribute]):
                outlier_dict[photo][static_attribute] = "Data not captured"

            else:
                normal_dict[photo][static_attribute] = photo_df.loc[photo,
                                                                    static_attribute]

    outliers_by_photo_df = pd.DataFrame.from_dict(outlier_dict, orient='index')
    outliers_by_photo_df_explanation = outliers_by_photo_df.replace(
        np.nan, "Data is in JSON/CSV file")
    normal_by_photo_df = pd.DataFrame.from_dict(normal_dict, orient='index')

    return outliers_by_photo_df, outliers_by_photo_df_explanation, normal_by_photo_df


def summary_of_flags_by_photo(outliers_by_photo_df):
    df_summary_of_flags_by_photo = pd.DataFrame()
    df_summary_of_flags_by_photo['Number of Flags'] = outliers_by_photo_df.count(
        axis='columns')
    df_summary_of_flags_by_photo.index.name = 'File Name'
    df_summary_of_flags_by_photo = df_summary_of_flags_by_photo.sort_values(
        by='Number of Flags', ascending=True)
    json_summary_of_flags_by_photo = df_summary_of_flags_by_photo.to_json(
        orient="split")
    df_summary_of_flags_by_photo_for_barchart = df_summary_of_flags_by_photo.reset_index()

    return df_summary_of_flags_by_photo, json_summary_of_flags_by_photo, df_summary_of_flags_by_photo_for_barchart


def outliers_barchart_by_photo(df_summary_of_flags_by_photo_for_barchart):
    barchart = alt.Chart(df_summary_of_flags_by_photo_for_barchart).mark_bar().encode(
        x='Number of Flags',
        y=alt.Y('File Name', sort='x')
    )

    return barchart


def check_consistency(pj):
    df = renaming_function_file(pj)
    # transpose_df = binning_function(df)
    # outliers_by_device_id_nested_df, outlier_dict, list_of_device_ID_occurences, device_occurence_dict, outlier_list = outliers_histogram_robust(
        # df, transpose_df)
    # chart = interactive_histogram(
    #     outliers_by_device_id_nested_df, outlier_list)
    # chart.show()
    # df_summary_of_flags, json_summary_of_flags_by_device = summary_of_flags(
    #     outliers_by_device_id_nested_df)
    photo_df = change_df_index(df)
    outliers_by_photo_df, outliers_by_photo_df_explanation, normal_by_photo_df = outliers_dataframe_by_photo_robust(
        photo_df)
    df_summary_of_flags_by_photo, json_summary_of_flags_by_photo, df_summary_of_flags_by_photo_for_barchart = summary_of_flags_by_photo(
        outliers_by_photo_df)
    if df_summary_of_flags_by_photo_for_barchart.empty:
        message = ("No missing data points!")
        barchart = pd.read_clipboard()

    else:
        barchart = outliers_barchart_by_photo(
            df_summary_of_flags_by_photo_for_barchart)
        barchart.show()

    return json.dumps({
        "outliers_by_photo_df_explanation": json.loads(outliers_by_photo_df_explanation.to_json()),
        # "df_summary_of_flags": json.loads(df_summary_of_flags.to_json()),
        "df_summary_of_flags_by_photo": json.loads(df_summary_of_flags_by_photo.to_json()),
        # "chart": chart.to_json(),
        "barchart": barchart.to_json()
    })


check_consistency(pj)
