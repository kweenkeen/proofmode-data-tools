# Proofcheck consistency and synchrony checks

- Install dependencies, found in requirements.txt

- Run on the command line:

  - Run the following command:

    `python main.py`

  - When prompted, add the batch JSON file name, including extension. (Make sure that all files are in the same folder!)

OR

- Open the notebook and follow the instructions
