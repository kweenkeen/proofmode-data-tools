altair==4.2.2
geopy==2.3.0
pandas==1.5.3
geopandas==0.12.2
numpy==1.24.2
folium==0.14.0
