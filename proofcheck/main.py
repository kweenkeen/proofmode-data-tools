from consistency import check_consistency
from synchrony import check_synchrony
import os
import json
from pathlib import Path
import re

# all outputs will be written to the "outputs" folder

json_batch_file = input(
    'Enter the relative path of the proof JSON batch file: ')
proofJSON = os.path.join(os.getcwd(), json_batch_file)

# consistency check
consistency_result = check_consistency(proofJSON)
parsed_consistency = json.loads(consistency_result)

with open("outputs/consistency_result.json", "w") as write_file:
    json.dump(parsed_consistency, write_file, indent=4)

vega_template = Path("inputs/vega-template.html").read_text()
chart_html = re.sub("VEGA_SPEC", parsed_consistency['chart'], vega_template)
Path("outputs/chart.html").write_text(chart_html)
barchart_html = re.sub(
    "VEGA_SPEC", parsed_consistency['barchart'], vega_template)
Path("outputs/barchart.html").write_text(barchart_html)


# synchrony check
synchrony_result = check_synchrony(proofJSON)
parsed_synchrony = json.loads(synchrony_result)

with open("outputs/synchrony_result.json", "w") as write_file:
    json.dump(parsed_synchrony, write_file, indent=4)

centroid_map_html = parsed_synchrony['centroid_map_html']
Path("outputs/centroid_map.html").write_text(centroid_map_html)
