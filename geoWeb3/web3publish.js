
// In newer Node.js versions where process is already global this isn't necessary.
import process from "process"
import path from "path"
import fs from "fs"
import { Web3Storage, getFilesFromPath } from 'web3.storage'

async function publishPath (apiToken, localPath) {

// Construct with token and endpoint
const client = new Web3Storage({ token: apiToken })

const files = await getFilesFromPath(localPath)

// Pack files into a CAR and send to web3.storage
const rootCid = await client.put(files) // Promise<CIDString>
// Get info on the Filecoin deals that the CID is stored in
const info = await client.status(rootCid) // Promise<Status | undefined>
// Fetch and verify files from web3.storage
const res = await client.get(rootCid) // Promise<Web3Response | null>
const pfiles = await res.files() // Promise<Web3File[]>
for (const file of pfiles) {
  console.log(`${file.cid}`)
  fs.writeFileSync(localPath + ".cid", file.cid);
}


}

async function main ()
{
var apiToken = process.argv[2]
var targetDir = process.argv[3]

var files = fs.readdirSync(targetDir)

for (const file of files) {
    // Make one pass and make the file complete
    var fromPath = path.join(targetDir, file);
 
      if (fromPath.endsWith(".zip"))
      {
            if (fs.existsSync(fromPath + ".cid"))
            {
		console.log("CID exists: " + fromPath + ".cid")
            }
            else 
            {
             console.log("publishing: " + fromPath)
	     await publishPath(apiToken, fromPath)
            }
      }
}

}

main()
